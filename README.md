# Sociedade dos Saberes Livres (SSL)

Repositório de planejamento e conteúdo do site da [**Sociedade dos Saberes Livres**](https://sabereslivres.org).

## Quem somos

## Nossos objetivos

## Licenciamento deste conteúdo

Por padrão, a menos que seja especificado o contrário, todo o conteúdo deste repositório está publicado sob os termos da licença [Creative Commons BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0/deed.pt_BR), o que significa que você pode copiar nosso conteúdo livremente, mas está obrigado a:

* nos dar o crédito apropriado;
* prover um link para a licença;
* e indicar claramente se mudanças foram feitas.

Você deve fazer isso em qualquer circunstância razoável, mas de nenhuma forma sugirindo que nós apoiamos você ou o uso que você fez do nosso conteúdo. Além disso, este modelo de licenciamento diz claramente que você **não pode distribuir o conteúdo deste repositório como se fosse nosso se ele for modificado**.

Nós adotamos e recomendamos esta licença para qualquer material que reflita opiniões e posicionamentos - o que constitui a maior parte do conteúdo do nosso site. Para materiais de uso prático, como software e trabalhos culturais ou educacionais livres, nós utilizamos e recomendamos a licença [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

